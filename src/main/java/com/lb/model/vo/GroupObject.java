package com.lb.model.vo;

import com.lb.model.user.User;
import lombok.Data;

import java.util.List;

@Data
public class GroupObject {

    List<UserGroup> userGroup;

    User user;

    public GroupObject(List<UserGroup> userGroup, User user) {
        this.userGroup = userGroup;
        this.user = user;
    }
}
