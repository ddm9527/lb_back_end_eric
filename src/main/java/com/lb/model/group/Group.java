package com.lb.model.group;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("lb_group")
public class Group implements Serializable {

    @TableId(type = IdType.AUTO)
    Integer id;

    String name;

    String avatar;

    Date createTime;

    Date updateTime;

}
