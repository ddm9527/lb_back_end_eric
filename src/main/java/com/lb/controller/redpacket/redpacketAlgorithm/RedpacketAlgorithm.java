package com.lb.controller.redpacket.redpacketAlgorithm;

import java.util.Random;

public class RedpacketAlgorithm {
    //红包算法
    public double[] divived(double money, int n){

        double[] array = new double[n];
        ArithUtil arithUtil = new ArithUtil();
        for (int i=0;i<n;i++){
            if(i==n-1){
                array[i]=money;
                break;
            }
            double a= getRandomMoney(n,money);

            array[i]=a;
            money=arithUtil.sub(money,a);
        }

        return array;
    }

    public  static double getRandomMoney(int remainSize,double remainMoney){
        //remainSize 剩余的红包数量
        //remainMoney 剩余的钱
        if(remainSize==1){
            remainSize--;
            return (double) Math.round(remainMoney*100)/100;
        }
        Random r = new Random();
        double min = 0.01;
        double max = remainMoney /remainSize *2;
        double money =r.nextDouble()*max;
        money=money<=min?0.01:money;
        money = Math.floor(money*100)/100;
        remainSize--;
        remainMoney-=money;

        return money;

    }
}
