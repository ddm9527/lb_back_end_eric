package com.lb.controller.redpacket;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lb.controller.criteria.*;
import com.lb.controller.redpacket.redpacketAlgorithm.RedpacketAlgorithm;
import com.lb.model.redpacket.PacketUUid;
import com.lb.model.redpacket.UserReceived;
import com.lb.model.user.User;
import com.lb.service.redpacket.PacketUUidService;
import com.lb.service.redpacket.UserReceivedService;
import com.lb.service.user.UserService;
import com.lb.util.WebsocketClient;
import com.lb.util.common.response.ResponseObj;
import com.lb.util.wsRunnable.WsRunnable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.Version;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/redpacket")
public class RedpacketController {

    @Autowired
    PacketUUidService packetUUidService;

    @Autowired
    UserService userService;
    /*UserAccountService userAccountService;*/

    @Autowired
    UserReceivedService userReceivedService;
    //被抢过红包的缓存
    Map<Integer,List<GetRedPacketList>> gettingUser = new HashMap<>();

    //用户账户缓存
    Map<Integer, User> userAccountMap = new HashMap<>();

    WebsocketClient wclient=new WebsocketClient();

    @Value("${wsSocket.hostIP}")
    public String hostIp;

    //发送红包金额
    Map<Integer,Double> sendRedPacketMoney = new HashMap<>();

    //抢到红包用户信息的缓存
    Map<OpenRedPacket,GetPacketMessge> getPacketMessgeMap = new HashMap<>();

    //该map缓存发送对应红包的用户信息
    Map<Integer,GetPacketMessge> messgeMap = new HashMap<>();

    boolean done = true;
    @RequestMapping(value = "/send", method = RequestMethod.POST)
    @Transactional
    @Version
    public ResponseObj send(@RequestBody RedpacketCriteria rpCriteria) throws Exception {
        if(!done) return ResponseObj.createResponse(-2,"服务器繁忙，请稍后重试");
        done=false;
        long start = System.currentTimeMillis();

        if(rpCriteria.getTotalNum()>50){
            done=true;
            return ResponseObj.createResponse(-2,"单次红包数量上限为50个");
        }
       Integer userId=rpCriteria.getUserId();
       //如果id为空则用户未登陆，请用户登陆之后继续操作
       if(userId==null){
           done=true;
           return ResponseObj.createResponse(-2,"发送红包异常，请登陆后继续操作");
       }
        //通过userId获得用户的钱包信息
        //===========================
        User user=userService.
                getOne(new QueryWrapper<User>().eq("id",userId));
       //判断用户是否存在
        if(user==null){
            done=true;
            return ResponseObj.createResponse(-2,"请登陆后尝试");
        }
        //得到用户账户余额
       double balance=user.getGoldCoin();
        //如果发送红包的金额大于用户钱包的金额，抛出提示
       if (balance<rpCriteria.getMoney()){
           done=true;
           return  ResponseObj.
                   createResponse(-2,"您的账户余额不足！");
       }
        //设置红包过期时间为1天
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH,+1);
        Date overdue=calendar.getTime();
        //如果发送红包

        //如果type==1 发送普通红包的操作
       if(rpCriteria.getType()==1){
           Integer nextId =  packetUUidService.nextId();
           String uuid = packetUUidService.UUid();
           PacketUUid packetUUid = new PacketUUid(userId,
                   nextId+1,uuid,0,rpCriteria.getMoney(), rpCriteria.getContent(),new Date(),overdue);

           boolean isTrue=true;
           do{
               try{
                   packetUUidService.save(packetUUid);
                   isTrue=false;
               }catch (Exception e){
                   uuid= packetUUidService.UUid();
                   packetUUid = new PacketUUid(userId,
                           nextId+1,uuid,0,rpCriteria.getMoney(),rpCriteria.getContent(),new Date(),overdue);
               }
           }while (isTrue);
           user.setGoldCoin(user.getGoldCoin()-rpCriteria.getMoney());
           userService.updateAccount(user.getGoldCoin(),user.getId());

           done=true;
           return ResponseObj.createResponse(0,"普通红包发送成功！");
       }
        //获取用户即将发送的红包个数
       Integer totalNum=rpCriteria.getTotalNum();
        //在此判断金币的数量按照每个红包为0.01的金额分配，是否能够足够分配
        double money = rpCriteria.getMoney();
        int count =(int)(money/0.01);
        if(rpCriteria.getTotalNum()>count){
            done=true;
            return
                    ResponseObj.createResponse(-2,"您发送的金币不足已分配"+rpCriteria.getTotalNum()+"个红包");
        }
        RedpacketAlgorithm redpacketAlgorithm = new RedpacketAlgorithm();
        double[]  array = redpacketAlgorithm.divived(money,totalNum);
        //生成红包唯一id
            Integer nextId= packetUUidService.nextId();
            if(nextId==null)nextId=0;

        //生成uuid
        String uuid = packetUUidService.UUid();
        List<PacketUUid> packetUUidList = new ArrayList<>();
            //生成红包算法
            for (int i=0;i<array.length;i++){
                //通过红包个数生成相对应的红包uuid
                PacketUUid packetUUid = new PacketUUid(userId,nextId+1
                        ,uuid,0,array[i],rpCriteria.getContent(),new Date(),overdue);

                boolean isTrue=true;
                do{
                    try{
                        packetUUidService.save(packetUUid);
                        packetUUidList.add(packetUUid);
                        isTrue=false;
                    }catch (Exception e){
                        uuid= packetUUidService.UUid();
                        packetUUid = new PacketUUid(userId,nextId+1
                                ,uuid,0,array[i],rpCriteria.getContent(),new Date(),overdue);
                    }
                }while (isTrue);
            }

        //成功发出红包之后更新用户中的账户余额
        user.setGoldCoin(user.getGoldCoin()-rpCriteria.getMoney());

        userService.updateAccount(user.getGoldCoin(),userId);

       /*.update(user.getGoldCoin(),rpCriteria.getUserId());*/

        sendRedPacketMoney.put(nextId+1,rpCriteria.getMoney());

        long end = System.currentTimeMillis();

        System.err.println("发红包总耗时："+(start-end));
        done=true;
        return ResponseObj.createResponse(0,"发送手气红包成功",
              nextId+1);
    }

    //打开红包
    @RequestMapping(value = "/openpacket",method = RequestMethod.POST)
    @Transactional
    public ResponseObj openPacket(@RequestBody OpenRedPacket openRedPacket){
       Integer id =openRedPacket.getPid();

        Integer userId =openRedPacket.getUId();

        User sendPacketUser =getSendbodyName(id);

        if(userId==null) return ResponseObj.createResponse(-2,"用户id不得为空！");

        Integer count =userService.userCount(userId);

        if(count==0) return ResponseObj.createResponse(-2,"用户不存在！");

        if(id==null)  return ResponseObj.createResponse(-2,"红包id不得为空！");

        long start = System.currentTimeMillis();

        List<PacketUUid> list=
            packetUUidService.list(new QueryWrapper<PacketUUid>().eq("packet_id",id));

        if(list.size()==0)return ResponseObj.createResponse(-2,"该红包不存在");

        long day =new Date().getTime()-list.get(0).getOverdueTime().getTime();

        day=day*24*60*60*100;

        if(day>=0) return ResponseObj.createResponse(-2,"该红包已过期",
                getPacketMessge(id));

        //获取红包中唯一的uuid
        String uuid=null;
        double money =0.00;
        String content=null;
        PacketUUid packetUUid = null;

        //遍历对应id的红包
        for(int i=0;i<list.size();i++){
            //如果该红包状态等于0说明该红包没有被拆过
            if(list.get(i).getHasUsed()==0){
                //获取该红包中唯一的uuid
                uuid =list.get(i).getPacketUuid();
                //获取该红包中的金额
                money = list.get(i).getPacketMoney();
                //获取对应红包中的红包寄语
                content=list.get(i).getContent();
                //将该红包的状态设置为1，已使用
                list.get(i).setHasUsed(1);

                packetUUid=list.get(i);
                //更新packetuuid中对应红包的状态为1
                break;
                }
            }
        //如果遍历完该红包列表之后没有获得uuid，说明该红包已经被抢完
        if(uuid==null&& getPacketMessgeMap.get(openRedPacket)==null)return ResponseObj.createResponse(-1,"该红包已抢完",
                getPacketMessge(id));

        if(uuid==null)return ResponseObj.createResponse(-1,"该红包已抢完",
                getPacketMessgeMap.get(openRedPacket));

        //抢到红包之后更新抢红包用户的账户余额

        User user= userService.getOne(new QueryWrapper<User>().eq("id",userId));
        if(user==null){
            return ResponseObj.createResponse(-2,"您的账户不存在或者您未登陆");
            }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //该数据库用于记录已被抢的红包
        UserReceived userReceived = new UserReceived(uuid,id,userId,content,money,simpleDateFormat.format(new Date()));
        try {
            //保存至数据库
            userReceivedService.save(userReceived);
            //如果用户存在，更新账户
            userService.updateAccount(user.getGoldCoin()+money
                    ,userId);
            packetUUidService.update(packetUUid,new QueryWrapper<PacketUUid>().eq("packet_uuid",uuid));
        }catch (Exception e){
            return ResponseObj.createResponse(-3,"您已领取过该红包，不可重复领取！"
                    ,getPacketMessgeMap.get(openRedPacket));
        }
            gettingUser.clear();
            //将已抢到的红包存进缓存
            back(userReceived.getId());

        long end = System.currentTimeMillis();

        System.err.println("抢红包总耗时:"+(end-start));

        GetPacketMessge getPacketMessge = new GetPacketMessge(uuid,money,sendPacketUser.getAccountName(),content,
                sendPacketUser.getProfilePicture(),
                simpleDateFormat.format(new Date()),
                money(openRedPacket.getPid()),list.size(),packetUUidService.noUserPacket(openRedPacket.getPid()));

        getPacketMessgeMap.put(openRedPacket,getPacketMessge);
        messgeMap.put(openRedPacket.getPid(),getPacketMessge);

        BroadcastWsBody bwb = new BroadcastWsBody(50, getPacketMessge.getSendPacketUser(), 1
                , user.getName()+" 抢到了"+getPacketMessge.getSendPacketUser()
                +"的红包"+getPacketMessge.getMoney()+"元！");
        wclient.connection(JSON.toJSONString(bwb),hostIp);

        return ResponseObj.createResponse(0,"开启红包",
                getPacketMessge);
        }


    //查看该红包已领取用户
    @RequestMapping(value = "/selectgetuser",method =RequestMethod.GET )
    public ResponseObj selectGetUser(@RequestParam Integer pId){

        if(gettingUser.get(pId)!=null)return
                ResponseObj.createResponse(0,"成功领取红包的用户",gettingUser.get(pId));

        return ResponseObj.createResponse(0,"成功领取红包的用户",back(pId));
    }

    //查看用户账户信息
    @RequestMapping(value = "/useraccount",method = RequestMethod.GET)
    public ResponseObj userAccount(@RequestParam Integer userId){
        User user=userAccountMap.get(userId);
        if(user==null){
            user=userService.getOne(new QueryWrapper<User>().eq("id",userId));
            userAccountMap.put(userId,user);
        }
        return ResponseObj.createResponse(0,null,user);
    }

    //通过红包id获得红包列表并且存入hashmap
   public List<GetRedPacketList> back(Integer pId){
       List<UserReceived>list=userReceivedService.list(pId);
       List<GetRedPacketList> back=new ArrayList<>();

       for (UserReceived userReceived:list) {
           double packetMoney=userReceived.getMoney();
           Integer UserId=userReceived.getUserId();
           String getTime = userReceived.getGetTime();
           User user =userService.user(userReceived.getUserId());
           GetRedPacketList getRedPacketList =
                   new GetRedPacketList(packetMoney,UserId,getTime,user.getAccountName(),
                           user.getProfilePicture());
           back.add(getRedPacketList);
       }
       gettingUser.put(pId,back);
       return gettingUser.get(pId);
   }

   //通过红包id查询到发送红包人的昵称
   public User getSendbodyName(Integer pId){
        List<Integer> userId =packetUUidService.UserId(pId);
        User userName = userService.user(userId.get(0));
        return userName;
   }

   //通过红包id查询发送红包人的信息
    public GetPacketMessge getPacketMessge(Integer pid){
        GetPacketMessge getPacketMessge =messgeMap.get(pid);
        if(getPacketMessge==null){
            return getPacketMessge;
        }
        getPacketMessge.setGetTime(null);
        getPacketMessge.setUuid(null);
        getPacketMessge.setMoney(0.00);

        return getPacketMessge;
    }

    public double money(Integer pId){
        Double sendMoney =  sendRedPacketMoney.get(pId);

        if(sendMoney==null) sendMoney=packetUUidService.sum(pId);

        sendRedPacketMoney.put(pId,sendMoney);

        return sendMoney;
    }

}
