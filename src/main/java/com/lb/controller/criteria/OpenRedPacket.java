package com.lb.controller.criteria;

import lombok.Data;

@Data
public class OpenRedPacket {
    Integer pid;

    Integer uId;
}
