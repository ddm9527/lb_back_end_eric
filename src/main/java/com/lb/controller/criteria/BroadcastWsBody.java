package com.lb.controller.criteria;

import lombok.Data;

/**
 * ws消息格式类：广播请求消息
 */
@Data
public class BroadcastWsBody {
    /**
     * cmd;
     */
    private int cmd;
    /**
     * 发送用户id;
     */
    private String from;
    /**
     * 群组id;
     */
    private Integer groupId; // 为空时则为全站广播
    /**
     * 消息内容;
     */
    private String content;

    public BroadcastWsBody() {
    }

    public BroadcastWsBody(int cmd, String from, Integer groupId, String content) {
        this.cmd = cmd;
        this.from = from;
        this.groupId = groupId;
        this.content = content;
    }

}
