package com.lb.controller.criteria;

import lombok.Data;

@Data
public class GroupIdenttyCriteria {

    Integer groupId;

    Integer memberId;

    Integer identity;

}
