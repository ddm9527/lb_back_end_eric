package com.lb.service.FeaturesObj.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.mapper.FeaturesObj.AnnouncementMapper;

import com.lb.model.FeaturesObj.Announcement;
import com.lb.service.FeaturesObj.AnnouncementService;
import org.springframework.stereotype.Service;

@Service
public class AnnouncementServiceImpl extends ServiceImpl<AnnouncementMapper, Announcement> implements AnnouncementService {

}
