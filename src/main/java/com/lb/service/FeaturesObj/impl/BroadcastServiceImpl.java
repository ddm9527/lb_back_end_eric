package com.lb.service.FeaturesObj.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.mapper.FeaturesObj.BroadcastMapper;
import com.lb.model.FeaturesObj.Broadcast;
import com.lb.service.FeaturesObj.BroadcastService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BroadcastServiceImpl extends ServiceImpl<BroadcastMapper, Broadcast> implements BroadcastService {
    @Override
    public List<Broadcast> usingBro() {
        return baseMapper.usingBro();
    }
}
