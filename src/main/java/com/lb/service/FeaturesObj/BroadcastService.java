package com.lb.service.FeaturesObj;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.model.FeaturesObj.Broadcast;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BroadcastService extends IService<Broadcast> {
    List<Broadcast> usingBro();
}
