package com.lb.service.FeaturesObj;

import com.baomidou.mybatisplus.extension.service.IService;

import com.lb.model.FeaturesObj.Announcement;
import org.springframework.stereotype.Service;

@Service
public interface AnnouncementService extends IService<Announcement> {
}
