package com.lb.service.impl.user;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.mapper.user.IpAddressMapper;
import com.lb.model.user.IpAddress;
import com.lb.service.user.IpAddressService;
import org.springframework.stereotype.Service;

@Service
public class IpAddressImpl extends ServiceImpl<IpAddressMapper, IpAddress> implements IpAddressService {
}
