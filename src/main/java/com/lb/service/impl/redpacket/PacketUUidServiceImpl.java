package com.lb.service.impl.redpacket;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.mapper.redpacket.PacketUUidMapper;
import com.lb.model.redpacket.PacketUUid;
import com.lb.service.redpacket.PacketUUidService;
import com.lb.util.WebsocketClient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PacketUUidServiceImpl extends ServiceImpl<PacketUUidMapper, PacketUUid> implements PacketUUidService {

    @Override
    public Integer nextId() {
        return baseMapper.nextId();
    }

    @Override
    public String UUid() {
        return baseMapper.UUid();
    }

    @Override
    public List<Integer> UserId(Integer packetId) {
        return baseMapper.userId(packetId);
    }

    @Override
    public Integer noUserPacket(Integer packetId) {
        return baseMapper.noUserPacket(packetId);
    }

    @Override
    public Double sum(Integer pId) {
        return baseMapper.sum(pId);
    }

    @Override
    public List<PacketUUid> selectOverdue() {
        return baseMapper.selectOverdue();
    }

    @Override
    public Integer counts(Integer packetId) {
        return baseMapper.counts(packetId);
    }

}
