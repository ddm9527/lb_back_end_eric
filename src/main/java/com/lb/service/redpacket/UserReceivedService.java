package com.lb.service.redpacket;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.model.redpacket.UserReceived;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserReceivedService extends IService<UserReceived> {
    List<UserReceived> list(Integer pId);
}
