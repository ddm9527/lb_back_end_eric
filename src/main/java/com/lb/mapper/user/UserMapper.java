package com.lb.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.user.User;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {
    @Update("update lb_user set gold_coin=#{money} where id=#{userId}")
    Integer updateAccount(double money,Integer userId);

    @Select("select count(*) from lb_user where id=#{userId}")
    Integer userCount(Integer userId);

    @Select("select account_name,profile_picture from lb_user where id=#{id}")
    User user(Integer id);

}
