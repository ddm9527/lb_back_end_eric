package com.lb.mapper.FeaturesObj;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.lb.model.FeaturesObj.Announcement;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

@Mapper
public interface AnnouncementMapper extends BaseMapper<Announcement> {

}
