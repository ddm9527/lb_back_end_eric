package com.lb.util.exceptionhandler;

import com.lb.util.common.response.ResponseObj;
import com.lb.util.exception.ImplException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * @author xiaoqiu
 * @title: GlobalExceptionHandler
 * @projectName ybadmin
 * @description: 全局异常处理 所有controller里面抛出的异常都到这里处理
 * <p>
 * 一次请求分成三个阶段，来分别进行全局的异常处理。
 * 一：在进入Controller之前，譬如请求一个不存在的地址，404错误。
 * 二：在执行@RequestMapping时，进入逻辑处理阶段前。譬如传的参数类型错误 405/415/400。
 * 三：以上都正常时，在controller里执行逻辑代码时出的异常。譬如NullPointerException。
 * @date 2019/6/17 10:37
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 400 - Bad Request
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseObj handleHttpMessageNotReadableException(Exception e) {
        logger.error("参数解析失败", e);
        return ResponseObj.createResponse(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
    }

    /**
     * 404- path not found
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseObj handleNoHandlerFoundException(NoHandlerFoundException e) {
        logger.error("请求地址找不到!", e);
        return ResponseObj.createResponse(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.getReasonPhrase());
    }

    /**
     * 405 - Method Not Allowed
     */
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseObj handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        logger.error("不支持当前请求方法", e);
        return ResponseObj.createResponse(HttpStatus.METHOD_NOT_ALLOWED.value(), HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase());
    }

    /**
     * 415 - Unsupported Media Type
     */
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler({HttpMediaTypeNotSupportedException.class, MethodArgumentTypeMismatchException.class})
    public ResponseObj handleHttpMediaTypeNotSupportedException(Exception e) {
        logger.error("不支持当前媒体类型", e);
        return ResponseObj.createResponse(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(), HttpStatus.UNSUPPORTED_MEDIA_TYPE.getReasonPhrase());
    }

    /**
     * 403 - forbidden
     */
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ExceptionHandler({UnauthorizedException.class, AuthorizationException.class})
    public ResponseObj authorizationErrorHandler(Exception e) {
        logger.error("无权访问", e);
        return ResponseObj.createResponse(-1,"该用户没有权限!");
    }

    /**
     * 登录异常处理方法
     *
     * @param e
     * @return
     */
    @ExceptionHandler({UnknownAccountException.class, LockedAccountException.class})
    public ResponseObj loginErrorHandler(Exception e) {
        return ResponseObj.createResponse(-1, e.getMessage());
    }

    /**
     * 自定义异常处理方法
     *
     * @param e
     * @return
     */
    @ExceptionHandler(ImplException.class)
    public ResponseObj implErrorHandler(Exception e) {
        return ResponseObj.createResponse(-1, e.getMessage());
    }

    /**
     * 非自定义异常默认处理方法
     *
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ResponseObj defaultErrorHandler(Exception e) {
        logger.error("exception", e);
        return ResponseObj.createResponse(-1, "操作失败!");
    }
}