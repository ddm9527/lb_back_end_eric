package com.lb.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.jcraft.jsch.*;

public class SSH2Util {

    private String host;

    private String user;

    private String password;

    private int port;

    private Session session;

    /**
     * 创建一个连接
     *
     * @param host     地址
     * @param user     用户名
     * @param password 密码
     * @param port     ssh2端口
     */
    public SSH2Util(String host, int port, String user, String password) {
        this.host = host;
        this.user = user;
        this.password = password;
        this.port = port;
    }

    private void initialSession() throws Exception {
        if (session == null) {
            JSch jsch = new JSch();
            session = jsch.getSession(user, host, port);
            session.setUserInfo(new UserInfo() {

                public String getPassphrase() {
                    return null;
                }

                public String getPassword() {
                    return null;
                }

                public boolean promptPassword(String arg0) {
                    return false;
                }

                public boolean promptPassphrase(String arg0) {
                    return false;
                }

                public boolean promptYesNo(String arg0) {
                    return true;
                }

                public void showMessage(String arg0) {
                }

            });
            session.setPassword(password);
            session.connect();
        }
    }

    /**
     * 关闭连接
     *
     * @throws Exception
     */
    public void close() throws Exception {
        if (session != null && session.isConnected()) {
            session.disconnect();
            session = null;
        }
    }

    /**
     * 上传文件
     * @param fullFilePath  本地路径
     * @param fileName  远程文件名
     * @param filePath  远程路径，若为空，表示当前根目录，若服务器上无此目录，则会自动创建
     * @param basePath  远程根目录
     * @throws Exception
     */
    public void putFile(String fullFilePath, String fileName, String filePath, String basePath)
            throws Exception {
        this.initialSession();
        Channel channelSftp = session.openChannel("sftp");
        channelSftp.connect();
        ChannelSftp c = (ChannelSftp) channelSftp;
        String remoteFile = null;

        mkdir(c, filePath, basePath);

//        String file = null;
//        if (localFile == null || localFile.trim().length() == 0) {
//            file = "*";
//        } else {
//            file = localFile;
//        }
//        if (localPath != null && localPath.trim().length() > 0) {
//            if (localPath.endsWith("/")) {
//                file = localPath + file;
//            } else {
//                file = localPath + "/" + file;
//            }
//        }
        c.put(fullFilePath, basePath + filePath + "/" + fileName);

        channelSftp.disconnect();
        this.close();
    }

    /**
     * 上传文件
     *
     * @param in        文件输入流
     * @param fileName  远程文件名
     * @param filePath  远程文件路径，若为空，表示当前路径，若服务器上无此目录，则会自动创建
     * @param basePath  远程基本路径
     * @throws Exception
     */
    public void putFile(InputStream in, String fileName, String filePath, String basePath)
            throws Exception {
        this.initialSession();
        Channel sftp = session.openChannel("sftp");
        sftp.connect();
        ChannelSftp c = (ChannelSftp) sftp;
        mkdir(c, filePath, basePath);
        String fullpath = basePath + filePath + "/" + fileName;
        System.out.println(fullpath);
        c.put(in, fullpath);

        sftp.disconnect();
        this.close();
    }

    private void mkdir(ChannelSftp sftp, String filePath, String basePath) {
        //如果目录不存在则创建目录
        String[] dirs = filePath.split("/");
        String tempPath = basePath;
        for (String dir : dirs) {
            if (null == dir || "".equals(dir)) continue;
            tempPath += "/" + dir;
            checkAndMkdir(sftp, tempPath);
        }
    }

    private void checkAndMkdir(ChannelSftp sftp, String folderName) {
        SftpATTRS attrs = null;
        try {
//            sftp.cd(folderName);
            attrs = sftp.stat(folderName);
        } catch (Exception e) {
            try {
                sftp.mkdir(folderName);
            } catch (Exception e1) {
            }
        }
    }

    public static void main(String[] args) throws Exception {
        SSH2Util ssh2Util = new SSH2Util("23.225.140.154", 22, "root", "Tj4530847@QAS");
        ssh2Util.putFile("C:/face1.gif", "22.gif", "/2019/11/13","/home/pic");
        return;
    }

    // public static void main(String[] args) {
    // Ssh2Util ssh = new Ssh2Util("192.168.1.189", "root", "test" , 22);
    // try {
    // String rs = ssh.runCommand("ddddd");
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // }
    // command 命令
//    public String runCommand(String command) throws Exception {
//        // CommonUtil.printLogging("[" + command + "] begin", host, user);
//
//        this.initialSession();
//        InputStream in = null;
//        InputStream err = null;
//        BufferedReader inReader = null;
//        BufferedReader errReader = null;
//        int time = 0;
//        String s = null;
//        boolean run = false;
//        StringBuffer sb = new StringBuffer();
//
//        Channel channel = session.openChannel("exec");
//        ((ChannelExec) channel).setCommand(command);
//        channel.setInputStream(null);
//        ((ChannelExec) channel).setErrStream(null);
//        err = ((ChannelExec) channel).getErrStream();
//        in = channel.getInputStream();
//        channel.connect();
//        inReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
//        errReader = new BufferedReader(new InputStreamReader(err, "UTF-8"));
//
//        while (true) {
//            s = errReader.readLine();
//            if (s != null) {
//                sb.append("error:" + s).append("\n");
//            } else {
//                run = true;
//                break;
//            }
//        }
//        while (true) {
//            s = inReader.readLine();
//            if (s != null) {
//                sb.append("info:" + s).append("\n");
//            } else {
//                run = true;
//                break;
//            }
//        }
//
//        while (true) {
//            if (channel.isClosed() || run) {
//                // CommonUtil.printLogging("[" + command + "] finish: " +
//                // channel.getExitStatus(), host, user);
//                break;
//            }
//            try {
//                Thread.sleep(1000);
//            } catch (Exception ee) {
//            }
//            if (time > 180) {
//                // CommonUtil.printLogging("[" + command + "] finish2: " +
//                // channel.getExitStatus(), host, user);
//                break;
//            }
//            time++;
//        }
//
//        inReader.close();
//        errReader.close();
//        channel.disconnect();
//        session.disconnect();
//        System.out.println(sb.toString());
//        return sb.toString();
//    }
}
