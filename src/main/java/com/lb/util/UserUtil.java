package com.lb.util;


import com.lb.model.user.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.stereotype.Component;

import java.io.Serializable;

import static org.apache.shiro.subject.support.DefaultSubjectContext.PRINCIPALS_SESSION_KEY;

@Component
public class UserUtil {

    public User getUserBytoken(String token){
        DefaultWebSecurityManager securityManager =(DefaultWebSecurityManager)SpringUtil.getBean(SecurityManager.class);
        DefaultWebSessionManager sessionManager = (DefaultWebSessionManager)securityManager.getSessionManager();
        Serializable serializable=token;
        Session session= null;
        try {
            session = sessionManager.getSessionDAO().readSession(serializable);
        } catch (UnknownSessionException e) {
            return null;
        }
        PrincipalCollection simplePrincipalCollection= (PrincipalCollection) session.getAttribute(PRINCIPALS_SESSION_KEY);
        User user=(User)simplePrincipalCollection.getPrimaryPrincipal();
        return user;
    }
}
