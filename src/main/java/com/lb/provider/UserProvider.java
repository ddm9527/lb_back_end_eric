package com.lb.provider;

import com.lb.model.user.User;

import java.util.Map;

public interface UserProvider {
    User checkLogin(String token);

    User flushUser(Integer id);
}
