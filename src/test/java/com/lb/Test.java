package com.lb;

public class Test {
    private static int start = 0;
    public static void main(String[] args) {
        int num = 100;
        while(num-->0){
            startAdd();
        }
        System.err.println(start);
    }
    public static void startAdd(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                start++;
            }
        }).start();
    }
}
